# Nginx + RTMP plugin enabled

## Introduction

This docker is compiled with x64 architecture, using i5 microprocessor. Moreover, this docker uses last stable nginx version (1.16.1) and last available rtmp-plugin version (1.2.1). Moreover, there are two container flavours: buster (\~304MB, just RTMP support) and Alpine (\~75MB just RTMP; \~ 130MB with RTMP and HLS support)
> NOTE: Images **are not** optimized for minimal size. See good example about this in [alfg/docker-nginx-rtmp](https://github.com/alfg/docker-nginx-rtmp).

## Get started

This server listen rtmp ingestion on port 1935 and serve connections via same port. This configuration can be changed with a new *nginx.conf* at build time.

## Nginx configuration
There are two nginx configurations, one default (with just *rtmp*) and another with *HLS* support.

### Default configuration (just rtmp)

The default *nginx.conf* used on *buster* and *alpine* is very simple:

```nginx

worker_processes auto;
rtmp_auto_push on;
events {}
rtmp {
    server {
        listen 1935;
        listen [::]:1935 ipv6only=on;    

        application live {
            live on;
            record off;
        }
    }
}

```
You can see just one application entry *live* for **rtmp** video input.

### HLS configuration (rtmp and HLS)

HLS(HTTP Live Streaming) is an HTTP-based adaptive bitrate streaming communications protocol developed by Apple Inc. and released in 2009. It's a widely used.
A modified *nginx2.conf* file is used on *buster-hls* (TODO Soon!) and *alpine-hls* tags:

```nginx
worker_processes auto;
rtmp_auto_push on;
events {worker_connections 1024}
rtmp {
    server {
        listen 1935;
        listen [::]:1935 ipv6only=on;    

        application live {
            live on;
            record off;
        }
    }
}

rtmp {
    server {
        listen 1935;
        listen [::]:1935 ipv6only=on; 
        chunk_size 4000;

        application test {
            live on;
            record off;
        }

        application stream {
            live on;

            exec ffmpeg -i rtmp://localhost:1935/$app/$name
              -c:a libfdk_aac -b:a 128k -c:v libx264 -b:v 2500k -f flv -g 30 -r 30 -s 1280x720 -preset superfast -profile:v baseline rtmp://localhost:1935/hls/$name_720p2628kbs
              -c:a libfdk_aac -b:a 128k -c:v libx264 -b:v 1000k -f flv -g 30 -r 30 -s 854x480 -preset superfast -profile:v baseline rtmp://localhost:1935/hls/$name_480p1128kbs
              -c:a libfdk_aac -b:a 128k -c:v libx264 -b:v 750k -f flv -g 30 -r 30 -s 640x360 -preset superfast -profile:v baseline rtmp://localhost:1935/hls/$name_360p878kbs
              -c:a libfdk_aac -b:a 128k -c:v libx264 -b:v 400k -f flv -g 30 -r 30 -s 426x240 -preset superfast -profile:v baseline rtmp://localhost:1935/hls/$name_240p528kbs
              -c:a libfdk_aac -b:a 64k -c:v libx264 -b:v 200k -f flv -g 15 -r 15 -s 426x240 -preset superfast -profile:v baseline rtmp://localhost:1935/hls/$name_240p264kbs;
        }

        application hls {
            live on;
            hls on;
            hls_fragment_naming system;
            hls_fragment 5;
            hls_playlist_length 10;
            hls_path /opt/data/hls;
            hls_nested on;

            hls_variant _720p2628kbs BANDWIDTH=2628000,RESOLUTION=1280x720;
            hls_variant _480p1128kbs BANDWIDTH=1128000,RESOLUTION=854x480;
            hls_variant _360p878kbs BANDWIDTH=878000,RESOLUTION=640x360;
            hls_variant _240p528kbs BANDWIDTH=528000,RESOLUTION=426x240;
            hls_variant _240p264kbs BANDWIDTH=264000,RESOLUTION=426x240;
        }
    }
}

http {
    access_log /dev/stdout combined;

    ssl_ciphers         HIGH:!aNULL:!MD5;
    ssl_protocols       TLSv1 TLSv1.1 TLSv1.2;
    ssl_session_cache   shared:SSL:10m;
    ssl_session_timeout 10m;

    server {
        listen 8080;

        # Uncomment these lines to enable SSL.
        # Update the ssl paths with your own certificate and private key.
        # listen 443 ssl;
        # ssl_certificate     /opt/certs/example.com.crt;
        # ssl_certificate_key /opt/certs/example.com.key;

        location /stat {
            rtmp_stat all;
            rtmp_stat_stylesheet static/stat.xsl;
        }

        # location /static {
        #     alias /www/static;
        # }

        location = /crossdomain.xml {
            root /www/static;
            default_type text/xml;
            expires 24h;
        }
    }
}
```

## Dockerfile

There are three *dockerfile* files: one for buster, other for alpine version of this container, and other with an special alpine version with HLS enabled. The version is in the building tag.


## Testing
For testing, simply you can stream rtmp video (For example one downloaded from [https://sample-videos.com](https://sample-videos.com)) from OBS to *rtmp://your_server_ip:1935/test/live*. Moreover, from a pc, you can launch a player; i.e.: *ffplay -framedrop rtmp://your_server_ip:1935/test/live*

## Credits / Bibliography

Based/Inspired on several others works:

* [tiangolo/nginx-rtmp-docker](https://www.github.com/tiangolo/nginx-rtmp-docker)
* [JasonRivers/Docker-nginx-rtmp](https://www.github.com/JasonRivers/Docker-nginx-rtmp)
* [shaneen31/nginx-rtmp-hls](https://github.com/Shaneen31/nginx-rtmp-hls/blob/master/Dockerfile)
* [arut/nginx-rtmp-module](https://github.com/arut/nginx-rtmp-module/wiki/Directives)
